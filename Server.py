import socket

# Create a socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
s.bind(("angsila.informatics.buu.ac.th", 8080))

# Listen for connections
s.listen(1)

# Accept a connection
conn, addr = s.accept()

# Get the username and password from the client
username = conn.recv(1024).decode()
password = conn.recv(1024).decode()

# Check if the username and password are correct
if username == "63160004" and password == "123456":

    # Open the file
    f = open("file.txt", "rb")

    # Read the contents of the file
    contents = f.read()

    # Write the contents of the file to the socket
    conn.sendall(contents)

    # Close the file
    f.close()

    # Close the socket
    conn.close()

else:

    # Print an error message
    print("Invalid username or password")

